﻿
GUI_ItemProperty_Load(aHandl)
{
    Global ItemProperty,gQZConfig
    ItemProperty:= New GUI2("ItemProperty", "+LastFound +Theme -DPIScale")
    ;~ ItemProperty.SetFont(QZGlobal.FontSize, "Microsoft YaHei")
    cons_fontsize:=strlen(gQZConfig.setting.global.DPIScale)?"s" . gQZConfig.setting.global.DPIScale:QZGlobal.FontSize
    ItemProperty.SetFont(cons_fontsize, "Microsoft YaHei")
    ItemProperty.AddCtrl("Edit_Item", "Edit", "x10 y10 w400 r18")
    ItemProperty.Show()
}

GUI_ItemProperty_LoadData(aObj)
{
    Global ItemProperty
    ItemProperty.Edit_Item.SetText(Json.Dump(aObj, 2))
}
