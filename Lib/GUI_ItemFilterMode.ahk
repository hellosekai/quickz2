﻿#Include %A_ScriptDir%\Lib\QZ_UpdategQZConfig.ahk

GUI_ItemFilterMode_Load(Callback, CloseEvent)
{
    Global ItemFilterMode, gQZConfig
    ItemFilterMode := new GUI2("ItemFilterMode", "+Lastfound +Theme -DPIScale")
    ;~ ItemFilterMode.SetFont(QZGlobal.FontSize, "Microsoft YaHei")
    cons_fontsize:=strlen(gQZConfig.setting.global.DPIScale)?"s" . gQZConfig.setting.global.DPIScale:QZGlobal.FontSize
    ItemFilterMode.SetFont(cons_fontsize, "Microsoft YaHei")
    ItemFilterMode.AddCtrl("TextTips", "Text",  "x10 y12 h28 ", "请选择后，使用右键菜单进行操作")
    ItemFilterMode.AddCtrl("LV_FilterMode", "ListView", "x10 y40 w360 r15 AltSubmit -hdr +grid","Title")
    ItemFilterMode.LV_FilterMode.OnEvent("GUI_ItemFilterMode_Event")
    ItemFilterMode.Show("","模式管理")
    ItemFilterMode.Callback := Callback
    If Strlen(CloseEvent)
        ItemFilterMode.OnClose(CloseEvent)
    FilterModeList := {}
    FilterModeList:=gQZConfig.setting.menuz.FilterModes
    LV_Delete()
    loop,% FilterModeList.MaxIndex()
        If strlen(FilterModeList[A_index])
            LV_Add("", FilterModeList[A_index])
    Return ItemFilterMode
}


GUI_ItemFilterMode_Dump()
{
    Global ItemFilterMode
    Return ItemFilterMode
}

GUI_ItemFilterMode_Destroy()
{
    Global ItemFilterMode
    ItemFilterMode.Destroy()
}

GUI_ItemFilterMode_Event()
{
    Global ItemFilterMode,gQZConfig
    If (A_GUIEvent  = "DoubleClick") && A_EventInfo
    {
        ;~ LV_GetText(strSelect, A_EventInfo)
        ;~ ItemFilterMode.Select := strSelect
        ;~ _Func := ItemFilterMode.Callback
        ;~ %_Func%()
        ;msgbox % A_EventInfo
    }
   if (A_GUIEvent = "RightClick" ) 
    {
        strSelect:=""
        LV_GetText(strSelect, A_EventInfo)
        ;~ msgbox % strSelect
        _hwnd := ItemFilterMode.hwnd
        MouseGetPos,CurX,CurY,,,,  ahk_id %_hwnd%
        Menu, RMenuFilterModeEdit, Add
        Menu, RMenuFilterModeEdit, DeleteAll
        Menu, RMenuFilterModeEdit, Add,新建模式, _MenuHandle_FilterModeEdit
        Menu, RMenuFilterModeEdit, Add,修改模式, _MenuHandle_FilterModeEdit
        if (strSelect="Title")
            Menu, RMenuFilterModeEdit,Disable,修改模式
        Menu, RMenuFilterModeEdit, Add,删除模式, _MenuHandle_FilterModeEdit
        if (strSelect="Title")
            Menu, RMenuFilterModeEdit,Disable,删除模式
        Menu, RMenuFilterModeEdit, Show, %CurX%, %CurY%
        Return
        _MenuHandle_FilterModeEdit:
        if(A_ThisMenuItem="新建模式")
        {
            ;提示输入信息并检验是否已存在
            InputBox, NewFilterMode, 输入, 请输入分类名称, , 200,120
            if ErrorLevel
                return
            if (NewFilterMode="")
                return
            FilterModeList := {}
            FilterModeList:=gQZConfig.setting.menuz.FilterModes
            loop,% FilterModeList.MaxIndex()
            {
                 If strlen(FilterModeList[A_index])
                {
                    if(NewFilterMode=FilterModeList[A_index])
                    {
                        MsgBox, 48, 提示, 当前模式：%NewFilterMode%`n`n已存在，请重新建立
                        return
                    }
                }
            }
            ;保存数据并刷新窗体
            FilterModeList.push(NewFilterMode)
            gQZConfig.setting.menuz.FilterModes:=FilterModeList
            ItemFilterMode.Default()
            LV_Delete()
            loop,% FilterModeList.MaxIndex()
                If strlen(FilterModeList[A_index])
                    LV_Add("", FilterModeList[A_index])
        } 
        else if(A_ThisMenuItem="修改模式")
        {
            ;提示输入信息并检验是否已存在
            InputBox, NewFilterMode, 输入, 请输入分类名称, , 200,120
            if ErrorLevel
                return
            if (NewFilterMode="")
                return
            FilterModeList := {}
            FilterModeList:=gQZConfig.setting.menuz.FilterModes
            loop,% FilterModeList.MaxIndex()
            {
                 If strlen(FilterModeList[A_index])
                {
                    if(NewFilterMode=FilterModeList[A_index])
                    {
                        MsgBox, 48, 提示, 当前模式：%NewFilterMode%`n`n已存在，请重新建立
                        return
                    }
                }
            }
            ;再次确认数据修改
            MsgBox, 4, 提示, 请确认以下数据：`n`n【原模式名称】：%strSelect%`n【现模式名称】：%NewFilterMode%
            IfMsgBox,No
                return
            ;更新所有数据内的分类
            ifMsgbox,Yes
                QZ_UpdateFilterMode(strSelect,NewFilterMode)
            ;保存数据并刷新窗体
            loop,% FilterModeList.MaxIndex()
            {
                 If strlen(FilterModeList[A_index])
                {
                    if(strSelect=FilterModeList[A_index])
                    {
                        FilterModeList[A_index]:=NewFilterMode
                        break
                    }
                }
            }
            gQZConfig.setting.menuz.FilterModes:=FilterModeList
            ItemFilterMode.Default()
            LV_Delete()
            loop,% FilterModeList.MaxIndex()
                If strlen(FilterModeList[A_index])
                    LV_Add("", FilterModeList[A_index])
        } 
        else if(A_ThisMenuItem="删除模式")
        {
            ;再次确认数据修改
            MsgBox, 4, 提示, 请确认删除以下类别：`n`n【类别名称】：%strSelect%
            IfMsgBox,No
                return
            ;更新所有数据内的分类
            FilterModeList := {}
            FilterModeList:=gQZConfig.setting.menuz.FilterModes
            ifMsgbox,Yes
                QZ_UpdateFilterMode(strSelect,"")
            ;保存数据并刷新窗体
            loop,% FilterModeList.MaxIndex()
            {
                 If strlen(FilterModeList[A_index])
                {
                    if(strSelect=FilterModeList[A_index])
                    {
                        FilterModeList.removeat(A_index)
                        break
                    }
                }
            }
            gQZConfig.setting.menuz.FilterModes:=FilterModeList
            ItemFilterMode.Default()
            LV_Delete()
            loop,% FilterModeList.MaxIndex()
                If strlen(FilterModeList[A_index])
                    LV_Add("", FilterModeList[A_index])
        } 
        Return
    }
}
