﻿Class QZGlobal
{
    Static Version := "2.1.2自用"
    Static Config := A_ScriptDir "\User\Config.json"
    Static OutsideEnv := A_ScriptDir "\User\Env.ini"
    Static Commands := A_ScriptDir "\Lib\Commands.json"
    Static FontSize := "s9"
    Static DefaultIcons := "`%Icons`%"
    Static DefaultIcl := A_ScriptDir "\User\Icons\Default.icl"
    Static TCMatchDll := A_ScriptDir "\Lib\TcMatch.dll"
    Static Line := "------------------"
    Static GroupIconFile := A_ScriptDir "\ICONS\Default.icl"
    Static GroupIconNumber := 16
    Static ItemNull := "[            ]"
    Static CrossCUR := A_ScriptDir "\Lib\Cross.CUR"
    Static KeywordList := ["本地", "网络", "搜索", "执行", "文件夹"]
    Static DisIconFile := A_ScriptDir "\User\Icons\Default.icl"
    Static DisIconNum := 39
    Static WinIconFile := A_ScriptDir "\User\Icons\Default.icl"
    Static WinIconNum := 14
    Static PluginDir := A_ScriptDir "\User\"
    Static InsideEnv :="A_ScriptDir|A_AhkPath|A_YYYY|A_MM|A_DD|A_MMMM|A_MMM|A_DDDD|A_DDD|A_WDay|A_YDay|A_YWeek|A_Hour|A_Min|A_Sec|A_MSec|A_Now|A_NowUTC|A_ComputerName|A_UserName|A_WinDir|A_ProgramFiles|A_AppData|A_AppDataCommon|A_Desktop|A_DesktopCommon|A_StartMenu|A_StartMenuCommon|A_Programs|A_ProgramsCommon|A_Startup|A_StartupCommon|A_MyDocuments|A_IPAddress1|A_IPAddress2|A_IPAddress3|A_IPAddress4"
    Static SystemEnv := "SystemRoot|ComSpec|LOCALAPPDATA|ProgramData|ProgramFiles|SystemDrive|TEMP" 
    ;Kawvin修改------------------------------
    static LabelList := ["【标签：文件】-------------------------","{file:path}  完整路径","{file:name}  文件名","{file:namenoext}  不含后缀的文件名","{file:dir}  目录路径","{file:ext}  文件后缀","{file:drive}  驱动器名","【标签：文件模板】---------------------","{file:[选项]}  格式化","【标签：选择】-------------------------","{select}  选择文本","{text}  选择的文本","{text:cp0}  所选文本，系统默认编码","{text:utf-8}  所选文本，utf8编码","【标签：日期时间】---------------------","{date:yyyyMMdd}  日期","{date:yyyy-MM-dd}  日期","{date:yyyy.MM.dd}  日期","{date:yyyyMMdd_HHmmss  日期时间}","{date:yyyy年MM月dd日HH时mm分  日期}","{date:yyyy年MM月dd日_HH时mm分ss秒  日期}","【标签：输入框】-----------------------","{input:提示信息}  显示获取用户输入，如用户名","{input:提示信息:hide}  隐式获取用户输入，如密码","【标签：文件/文件框】------------------","{box:filebrowser}  选择文件对话框","{box:mFilebrowser}  选择多文件对话框","{box:folderbrowser}  选择文件夹对话框","【标签：自定义】-----------------------"]
    static EvPath:=A_ScriptDir "\Apps\Everything\Everything_x64.exe"
    static EVDLL:=A_ScriptDir "\Lib\Everything.dll"
    static EVDLL64:=A_ScriptDir "\Lib\Everything64.dll"
}
    
