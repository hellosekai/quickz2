﻿/*
API=CustomFunc_RunD
Tips=拖放文件到程序窗口
Author=万年书妖、卢霖
Version=0.1
*/

/*
    函数: CustomFunc_RunD
        拖放文件到程序窗口

    参数:
		ExePath, WinTitle, [ x:=0, y:=0, WaitDuration:=0, StartNewInstance:=0, Files2Drop:="" ]
        程序路径, 程序标题, [ x 坐标, y 坐标, 超时等待时长, 是否尝试新开进程, 要拖放的文件路径 ]

    返回:
        不返回数据

    作者: 万年书妖、卢霖

    版本: 0.1
*/

CustomFunc_RunD(ExePath,WinTitle,x:=0,y:=0,WaitDuration:=0,StartNewInstance:=0,Files2Drop:="")
{ ;格式为RunD(应用程序,应用程序的标题,x,y,等待时间)
	;~ Global gMenuZ
	if not Files2Drop
		Files2Drop:=CustomFunc_QZSel().files
	If StartNewInstance or not WinExist(WinTitle)
	{
		Run "%ExePath%"
		Sleep,% (WaitDuration="") ? 1000 : WaitDuration
	}
	else
		WinActivate, %WinTitle%
	WinWaitActive, %WinTitle% ,,5
	WinActivate, %WinTitle%
	x:=x ? x : 100
	y:=y ? y : 100
	PostMessage, 0x233, CustomFunc_HDrop(Files2Drop,x,y), 0,, %WinTitle%
}