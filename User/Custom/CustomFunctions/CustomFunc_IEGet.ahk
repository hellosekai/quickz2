﻿CustomFunc_IEGet(Name="")      ; 获取到现有的 IE 窗口/选项卡的指针
{
   IfEqual, Name,, WinGetTitle, Name, ahk_class IEFrame
      Name := ( Name="New Tab - Windows Internet Explorer" ) ? "about:Tabs"
      : RegExReplace( Name, " - (Windows|Microsoft) Internet Explorer" )
   For Pwb in ComObjCreate( "Shell.Application" ).Windows
      If ( Pwb.LocationName = Name ) && InStr( Pwb.FullName, "iexplore.exe" )
         Return Pwb
} ;written by Jethrow