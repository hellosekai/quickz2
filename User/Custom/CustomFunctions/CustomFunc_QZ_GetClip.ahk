﻿CustomFunc_QZ_GetClip(ByRef aData)
{
    Global gQZConfig ;, QuickZ_ClipSave, QuickZ_Clip
    ;~ method  := gQZConfig.Setting.MenuZ.Method
    ;~ timeout := gQZConfig.Setting.MenuZ.Timeout
    method  := gQZConfig.Setting.global.Method      ;Kawvin修改------
    timeout := gQZConfig.Setting.global.Timeout         ;Kawvin修改------
    If Not TimeOut
        TimeOut := 0.6
    ; 监听剪切板
    CB := ClipboardAll
    sleep 180
    Clipboard := ""
    ;~ if WinActive("ahk_class TTOTAL_CMD")        ;如果是tc的窗体           ;Kawvin修改----------------------
    ;~ {
        ;~ sleep,30
        ;~ PostMessage,1075,2018,0,,ahk_class TTOTAL_CMD   ;复制所选文件全路径到剪贴板     ;Kawvin修改---------------------
        ;~ ClipWait,% timeout, 1
        ;~ MySel:=Clipboard
        ;~ MySel:=RegExReplace(MySel,"m)\\{1}$")           ;删除TC复制路径里路径最后所带的\
        ;~ aData.Files := MySel
            ;~ strFile := aData.Files
            ;~ aData.FileExt := QZ_GetFileExt(aData.Files)      ;调用Ahk文件：本文件，作用：获取文件类型或后缀名
            ;~ If InStr(aData.Files, "`n")
            ;~ {
                ;~ Loop, Parse, strFile, `n, `r
                ;~ {
                    ;~ strFile := A_LoopField
                    ;~ SplitPath, strFile, strFileName, strFileDir
                    ;~ aData.FileDir := strFileDir
                    ;~ Break
                ;~ }
                ;~ aData.FileMulti := QZ_GetFileMulti(aData.Files)
            ;~ } Else  {
                ;~ SplitPath, strFile, strFileName, strFileDir
                ;~ aData.FileName := strFileName
                ;~ aData.FileDir := strFileDir
            ;~ }
        
    ;~ } 
    ;~ else {
        if( method = 1 )
            SendInput, ^{Ins}
        else
            SendInput, ^c ;^{vk43sc02E} ;ctrl+c
        ClipWait,% timeout, 1
        ErrorCode := !Errorlevel ; 超时  
        if DllCall( "IsClipboardFormatAvailable", "UInt", iFmt:=15)
        {
            ;~ aData.text := ""
            aData.Files := Clipboard
            strFile := aData.Files
            ;~ msgbox % strFile
            aData.FileExt := QZ_GetFileExt(aData.Files)      ;调用Ahk文件：本文件，作用：获取文件类型或后缀名
            If InStr(aData.Files, "`n")
            {
                Loop, Parse, strFile, `n, `r
                {
                    strFile := A_LoopField
                    SplitPath, strFile, strFileName, strFileDir
                    aData.FileDir := strFileDir
                    Break
                }
                aData.FileMulti := QZ_GetFileMulti(aData.Files)
            } Else  {
                SplitPath, strFile, strFileName, strFileDir
                aData.FileName := strFileName
                aData.FileDir := strFileDir
            }
        }
        else  {
            ;~ aData.Files := ""
            aData.Text := Clipboard
        }
    ;~ }    
    ;QuickZ_Clip := Clipboard
    ;QuickZ_ClipSave := ClipboardAll
    Clipboard := CB
    return ErrorCode ; 获取到数据返回True、超时返回False
}