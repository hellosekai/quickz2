﻿/*
Filter=CustomFilter_FileType_IrfanView
Tips=此函数当所选仅包含 IrfanView 支持的文件时返回 true
Author=LL
Version=0.1
*/

CustomFilter_FileType_IrfanView(FileList="")
{	
    ExtNameSupported=folder;tif;ani;cur;avi;wmv;asf;b3d;bmp;dib;rle;cam;cdr;cmx;clp;cpt;crw;cr2;dcm;acr;ima;dcr;dng;eff;mrw;nef;nrw;orf;pef;raf;srf;arw;rwl;rw2;x3f;mpo;3fr;iiq;mos;dds;blp;djvu;iw44;dwg;dxf;hpgl;cgm;svg;plt;ecw;pdf;eps;ps;ai;exe;dll;cpl;exr;epx;g3;gif;hdp;jxr;wdp;ico;icl;ics;ids;iff;lbm;img;jls;jp2;jpg;jpeg;jpm;kdc;med;mng;jng;mp3;m3u;mov;mpg;mpeg;dat;ogg;pbm;pgm;ppm;pcd;pcx;dcx;pdn;png;psd;psp;ra;ras;raw;yuv;sff;sfw;sgi;sid;swf;flv;tga;ttf;wav;mid;rmi;wma;aif;snd;au;cda;wbmp;wbz;wbc;webp;wmf;emf;wsq;xbm;xpm;xcf
	;枚举所有支持的文件类型，正常状态下只需要修改筛选函数名以及支持的文件后缀即可（第2行、第8行、第10行）
	if not FileList
	{
		Global gMenuZ
		FileList=% gMenuZ.Data.files
	}
	Result:=true ;先假定为真
	if (FileList="")
		Return false ;kawvin修改
	;~ IfInString,FileList,`n
	{
		Loop, Parse, FileList, `n, `r
		{
			FileAttrib:=FileExist(A_LoopField)
			IfInString,FileAttrib,D
				continue
            SplitPath, A_LoopField, OutFileName, OutDir, OutExtension, OutNameNoExt, OutDrive
            IfNotInString, ExtNameSupported, %OutExtension%
            {
				Result:=false
				break
            }
		}
	}
	Return Result
}