﻿#Include %A_ScriptDir%\Lib\TrayIcon.ahk
#Include %A_ScriptDir%\Lib\WinClipAPI.ahk
#Include %A_ScriptDir%\Lib\WinClip.ahk
/*
Plugin=DM_SendFilesByQQ
Name1=通过QQ发送文件
Command1=DM_SendFilesByQQ
Author=Kawvin
Version=0.2版本
说明：批量添加，由|分开,=为分隔符,如：张三|李四||王五
*/

DM_SendFilesByQQ(aParam){    ; 此Plugin执行的command为Test，代表执行Test函数,函数请预留一个aParam，用于QuickZ传递参数过来。
	icon:=A_ScriptDir . "\User\Icons\QQ.ico"
    ;Menu_SendFilesByQQ := MenuZ_GetSibling() ; 获取一个菜单对象，不是子菜单，是同级菜单
	Menu_SendFilesByQQ := MenuZ_GetSub() ; 获取一个子菜单对象
	Menu_SendFilesByQQ.SetParams({iconsSize:20})
	IndexArray:=["1","2","3","4","5","6","7","8","9","0","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
	Menu_SendFilesByQQ.Add({name:"&1  发送到  我的Android手机",icon: icon,uid:{Handle:"DM_SendFilesByQQ_Handle",Data:"My_Android"}}) ; 安卓手机，苹果手机注释本行
	Menu_SendFilesByQQ.Add({name:"&2  发送到  我的iPhone",icon: icon,uid:{Handle:"DM_SendFilesByQQ_Handle",Data:"My_Apple"}})  ; 苹果手机，安卓手机注释本行
	Menu_SendFilesByQQ.Add()
	if !strlen(aParam)
		Menu_SendFilesByQQ.Add({name:"批量添加，由|分开,||为分隔符,如：张三|李四||王五,",icon: icon,uid:{Handle:"DM_SendFilesByQQ_Handle",Data:""}}) 
	Index:=3
	loop,parse, aParam,`|
	{
		MenuString:=trim(A_LoopField)
		if (MenuString!="") 
		{
			Menu_SendFilesByQQ.Add({name:"&" Index "  发送到  " MenuString,icon: icon,uid:{Handle:"DM_SendFilesByQQ_Handle",Data:MenuString}}) ; Handle的值为点击菜单后执行的功能
			Index+=1
		}
		else
			Menu_SendFilesByQQ.Add()
	}
	Menu_SendFilesByQQ.Add()
	Menu_SendFilesByQQ.Add({name:"&Z  搜索联系人",icon: icon,uid:{Handle:"DM_SendFilesByQQ_Handle",Data:"搜索联系人"}}) 
    return Menu_SendFilesByQQ  ; 必须返回子菜单对象
}

DM_SendFilesByQQ_Handle(aMsg, aObj)
{
    If (aMsg = "OnRun")
		Menu_SendFilesByQQ_Action(aObj.Uid.Data)
	;~ else If (aMsg = "OnRButton")			
	;~ else If (aMsg = "onmbutton")
}

Menu_SendFilesByQQ_Action(QQName)
{
	if (!strlen(QQName))
		return
	MySel:=QZData("files")
	;准备搜索数据
	if(QQName="My_Android")
		TemQQName:="Android"
	else if(QQName="My_Apple")
		TemQQName:="iPhone"
	else if(QQName="搜索联系人")
	{
		InputBox,TemQQName,请输入搜索内容,,,200,100
		if ErrorLevel
			return
		if (TemQQName="")
			return
	} else {
		TemQQName:=QQName
	}
	;获取QQ进程数量
	QQCount:=0
	for process in ComObjGet("winmgmts:").ExecQuery("Select * from Win32_Process")
	{
		if (process.Name="QQ.exe")
			QQCount+=1
	}
	;等待激活QQ
	if (QQCount>1)
	{
		TrayTip, 提醒,  选择需传送文件的QQ号!
		while 1
		{
			WinGetTitle, Cur_Title, A
			if (Cur_Title = "QQ")
				break
		}
	} else {
		BlockInput, on
		TrayIcon_Button("QQ.exe","L")
		sleep,500
		;~ TrayIcon_Button("QQ.exe","R")
		;~ sleep,200
		;~ send,{up}
		;~ send,{up}
		;~ send,{up}
		;~ send,{enter}
		BlockInput, off
	}
	WinActivate, QQ
	WinWaitActive, QQ ,,5
	;~ ControlClick, x20 y135, QQ,,,, Pos	;相对于窗体左上角的相对位置点击
	WinGetPos,Winx,Winy,,, QQ
	ControlClick, x%Winx% y%Winy%, QQ
	sendinput,{Esc}{esc}{esc}{esc}{esc}		;跳出已有搜索
	sleep,50
	Clipboard:=TemQQName
	sleep,50
	send,^{vk56}	;粘贴
	Clipboard=
	;sendinput,% MyFun_uStrQ(TemQQName)
	;~ sendinput,% MyFun_getAscStrQ(TemQQName)
	sleep,300
	send,{enter}		;确认搜索内容
	sleep,500
	SetTitleMatchMode,2
	if(QQName!="搜索联系人")
	{
		WinActivate, %TemQQName%		;通常不需要在 WinActivate 后使用 WinWaitActive 或 IfWinNotActive
		WinGetPos,Winx,Winy,,, %TemQQName%
		ControlClick, x%Winx% y%Winy%, %TemQQName%
	}

	sleep,200	
	loop,parse,MySel,`n,`r
	{
		WinClip.AppendFiles(A_LoopField)		;通过WinClip类将文件置入剪贴板
		sleep,100
	}
	send,^{vk56}	;粘贴文件
	;Clipboard=
	return
}

MyFun_uStrQ(str)
{
	charList:=StrSplit(str)
	SetFormat, integer, hex
	for key,val in charList
		out.="{U+ " . ord(val) . "}"	
	return out
}

MyFun_getAscStrQ(str)
{
    charList:=StrSplit(str)
    for key,val in charList
        out.="{Asc " . asc(val) . "}"
    return out
}