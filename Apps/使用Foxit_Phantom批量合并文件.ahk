﻿;~ ^f12::
FileList = %1%
gui Destroy
gui New
;~ gui +AlwaysOnTop
Gui Font, s12
Gui Add, Text, x10 y15 w85 h26 +0x200, 文件目录：
Gui Add, Edit, x100 y15 w384 h26 vMyVar_FileDir
Gui Add, Button, x490 y15 w70 h26, 目录浏览
Gui Add, ComboBox, x100 y50 w160 vMyVar_FileExt,pdf|jpg|png|bmp 
if strlen(FileList)
	gui Add,Checkbox,x320 y50 w160 h26 checked vMyVar_IsFullPath,文件列表为全路径
else
	gui Add,Checkbox,x320 y50 w160 h26 vMyVar_IsFullPath,文件列表为全路径
Gui Add, Text, x10 y47 w85 h30 +0x200, 文件格式：
Gui Add, Edit, x10 y85 w550 h500 vMyVar_FileList,%FileList%
Gui Add, Button, x110 y600 w75 h39, 文件合并
Gui Add, Button, x250 y600 w75 h39, 清空
Gui Add, Button, x390 y600 w75 h39, 退出
Gui Font
Gui Show, w570 h650, 批量合并文件
Return

Button目录浏览:
	FileSelectFolder, MyVar_FileDir
	if ErrorLevel
		return
	if (MyVar_FileDir="")
		return
	MyVar_FileDir := RegExReplace(MyVar_FileDir, "\\$")  ; 移除默认的反斜线,如果存在.
	guicontrol,,MyVar_FileDir,%MyVar_FileDir%
return

Button文件合并:
	gui,Submit
	NotExistList:=
	ExistList:=
	if MyVar_IsFullPath
	{
		loop,Parse,MyVar_FileList,`n,`r
		{
			TemFileFullPath:=trim(A_LoopField)
			if (TemFileFullPath="")
				continue
			ifnotexist,%TemFileFullPath%
				NotExistList.=TemFileFullPath "`n" 
			else
				ExistList.=TemFileFullPath "`n" 
		}
	} else {
		if (MyVar_FileExt="")
			msgbox ,请选择文件后缀名！
		loop,Parse,MyVar_FileList,`n,`r
		{
			TemFileNameNoExt:=trim(A_LoopField)
			if (TemFileNameNoExt="")
				continue
			TemFileFullPath=%MyVar_FileDir%\%TemFileNameNoExt%.%MyVar_FileExt%
			ifnotexist,%TemFileFullPath%
				NotExistList.=TemFileNameNoExt "." MyVar_FileExt "`n" 
			else
				ExistList.=TemFileFullPath "`n" 
		}
	}
	NotExistList:=RegExReplace(NotExistList,"\s*$","")   ;顺便删除最后的空白行，可根据需求注释掉
	ExistList:=RegExReplace(ExistList,"\s*$","")   ;顺便删除最后的空白行，可根据需求注释掉
	if strlen(NotExistList)
	{
		Clipboard=%NotExistList%
		MsgBox, 4, 提示, 以下文件不存在，是否继续？`n文件列表已复制到剪贴板，如有需要，请先粘贴保存`n`n%NotExistList%
	}
	IfMsgBox ,no
		goto ,Button退出
	
	INIFile=%A_ScriptDir%\%A_ScriptName%.ini
	StringReplace,INIFile,INIFile,`.ahk,,All
	StringReplace,INIFile,INIFile,`.exe,,All
	iniread,AppPath,%INIFile%,通用设置,程序路径
	ifnotexist,%AppPath%
	{
		AppPathC:="C:\Program Files (x86)\foxit software\foxit phantom\Foxit Phantom.exe"
		AppPathD:="D:\Program Files (x86)\foxit software\foxit phantom\Foxit Phantom.exe"
		AppPathE:="E:\Program Files (x86)\foxit software\foxit phantom\Foxit Phantom.exe"
		AppPathF:="F:\Program Files (x86)\foxit software\foxit phantom\Foxit Phantom.exe"
		ifexist,%AppPathC%
			AppPath:=AppPathC
		ifexist,%AppPathD%
			AppPath:=AppPathD
		ifexist,%AppPathE%
			AppPath:=AppPathE
		ifexist,%AppPathF%
			AppPath:=AppPathF
	}
	
	ifnotexist,%AppPath%
	{
		FileSelectFile, AppPath, 3, , 选择Foxit Phantom主程序路径, 程序文件(*.exe)
		if ErrorLevel
			return
		if (AppPath !="")
			IniWrite,%AppPath%,%INIFile%,通用设置,程序路径
		else
			return
	}
	run,"%AppPath%"
	sleep,2000
	#WinActivateForce
	WinActivate,,ahk_class classFoxitPhantom
	;~ sleep,1500
	;~ sendinput,!f
	;~ sleep,100
	;~ sendinput,f
	;~ sleep,100
	;~ sendinput,m
	WinMenuSelectItem, A, , 文件, 创建PDF, 从多文档
	WinMenuSelectItem, M, , 文件, 创建PDF, 从多文档
	sendinput,{tab}
	loop,Parse,ExistList,`n,`r
	{
		TemFileFullPath:=trim(A_LoopField)
		sendinput,{enter}
		sleep,300
		Clipboard=%TemFileFullPath%
		sendinput,^v
		sleep,300
		sendinput,{enter}
	}
	sendinput,!m
	goto ,Button退出
return

Button清空:
	guicontrol,,MyVar_FileDir,
	guicontrol,,MyVar_FileList,
	guicontrol,,MyVar_IsFullPath,0
	guicontrol,,MyVar_FileExt,||
	guicontrol,,MyVar_FileExt,pdf|jpg|png|bmp 
	MyVar_FileDir:=
	MyVar_FileList:=
	MyVar_FileExt:=
return

Button退出:
GuiEscape:
GuiClose:
    ;~ gui,Destroy
	ExitApp