﻿#NoEnv 
#SingleInstance, Force
SetBatchLines, -1
SendMode Input
SetWorkingDir %A_ScriptDir%
CoordMode, Mouse, Screen
SetKeyDelay, -1
SetControlDelay,-1
;RunAsAdmin()
global PUMCount:=0		 ;Kawvin修改
global PUMFirst:={}		 ;Kawvin修改
global LastIsSplit:=0		;Kawvin修改
QZ_Tray()		;调用Ahk文件：QZ_Tray.ahk，作用：绘制托盘图标
QZ_Main()		;调用Ahk文件：QZ_Main.ahk，作用：读取配置文件
QZ_MenuZ()	 ;调用Ahk文件：QZ_MenuZ.ahk，作用：

;~ xhotkey("!q", "MenuZALL")	 ;调用Ahk文件：xHotkey.ahk，作用：注册热键
;~ xhotkey("!w", "MenuZWin")	 ;调用Ahk文件：xHotkey.ahk，作用：注册热键

;~ CheckHotkey()
for hk , objHK in gQZConfig.hotkeys
{
	MyAction:=ObjHK.action
	xhotkey(hk,MyAction)
}
QZ_VimD()	 ;调用Ahk文件：QZ_VimD.ahk，作用：
return

;~ CheckHotkey()
;~ {
    ;~ global gQZConfig
	;~ MsgBox % gQZConfig.hotkeys.name
    ;~ If (strlen(gQZConfig.hotkeys[1].action)=0)
	;~ If (strlen(gQZConfig.hotkeys[1].action)=0)
	;~ If Not IsObject(gQZConfig["hotkeys"])
    ;~ {
		;~ gQZConfig.hotkeys["!z"] := {Action: "MenuZAll", Comment: "显示常规菜单", FilterMode: "", Sep: "0"}
        ;~ QZ_WriteConfig(gQZConfig,QZGlobal.Config)
		;~ gQZConfig := QZ_ReadConfig(QZGlobal.Config) 
		;~ ExitApp
    ;~ }
;~ }

#Include <QZ_MenuZ>
#Include <QZ_API>
#Include <Class_Json>
#Include <PUM>
#Include <PUM_API>
#Include <Path_API>
#Include <Class_QZGlobal>
#Include <Class_QZLang>
#Include <Class_VimDesktop>
#Include <QZ_Plugins>
#Include <QZ_Codes>
#Include <TT>
#Include <Class_ExcelStatic>
#Include <VIMD_Excel>